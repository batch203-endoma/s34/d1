//Load the expressjs module into our application and saved it in a variable called express
// require("package")
const express = require("express");

// localhost port number
const port = 4000;
//app is our server
// create an application that uses express and stores it as app
const app = express();

//middleware
//express.json() is a method which allow us to handle the streaming of data and automatical parse the incoming JSON.
app.use(express.json());

//mock data

let users = [{
    username: "TStark3000",
    email: " starkindustries@mail.com",
    password: "notPeterParker"
  },
  {
    username: "ThorThunder",
    email: " loveAndThunder@mail.com",
    password: "iLoveStormbreaker"
  }
];

let items = [{
    name: "Mjolnir",
    price: 50000,
    isActive: true
  },
  {
    name: "Vibranium Shield",
    price: 70000,
    isActive: true
  }
];

// Express has methods to use as routes corresnponding to HTTP methods
// Syntax: app.method (<endpoint>, <function for req and res>)

app.get(`/`, (req, res) => {
  //res.send() actually combines writeHead() and end()
  res.send(`Hello from my first expressJS API`);
  //res.status(200).send("message");
});

/*
Mini Activity : 5 mins

>> Create a get route in expressjs which will be able to send a messaage in the client:

  >> endpoint: /greeting
  >> message: `Hello from Batch203-surname`

>> Test in postman
>> send your response in hangouts

*/

app.get(`/greeting`, (req, res) => {
  res.send(`Hello from Batch203-endoma`);
});

//retrieval of the users in mock database
app.get(`/users`, (req, res) => {
  //res.send already stringifies for you.
  res.send(users);
});


// adding of new users

app.post(`/users`, (req, res) => {
  console.log(req.body); //
  //simulate creating a new user document
  let newUser = {
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  };
  //adding newUser to the existing array
  users.push(newUser);
  console.log(users);
  //send the updated users array in the client
  res.send(users);
})

// Delete method
app.delete(`/users`, (req, res) => {
  //deleting the last user of the users array
  users.pop();
  res.send(users);
});

//PUT method
//update user's password
//:index - wildcard
//url: localhost:400/users/0

app.put(`/users/:index`, (req, res) => {

  console.log(req.body); // updated password
  //result:{}

  //an object that contains the value of URL params
  console.log(req.params); //refer to the wildcard in the endpoint
  //Result: {index:0}

  //parseInt the value of the number coming from req.params
  //[`0`]

  let index = parseInt(req.params.index);
  //users[0].password
  //"notPeterParker" = "tonyStark"
  // the update will be coming from the request body
  users[index].password = req.body.password;
  //send the updated users
  res.send(users[index]);

});

//
// Activity : 30 mins
//
//     Endpoint: /items
//
//     >> Create a new route to get and send items array in the client (GET ALL ITEMS)

app.get(`/items`, (req, res) => {
  res.send(items)
});
//
//     >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
//         >> send the updated items array in the client
//         >> check the post method route for our users for reference
//
app.post(`/items`, (req, res) => {
  let newItems = {
    name: "Eyepatch",
    price: 100,
    isActive: true
  }
  items.push(newItems);
  console.log(items);
  res.send(items)
})

//     >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
//         >> pass the index number of the item that you want to update in the request params
//         >> add the price update in the request body
//         >> reassign the new price from our request body
//         >> send the updated item in the client

app.put(`/items/:index`, (req, res)=>{

  let x = parseInt(req.params.index);

  items[x].price = req.body.price;

  res.send(items[x]);

})


//     >> Create a new collection in Postman called s34-activity
//     >> Save the Postman collection in your s34 folder












//port listener
app.listen(port, () => console.log(`server is running at port ${port}`));
